$(document).on('click', '.btn', 'click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    fetch(url)
    .then((response) => {
      
      return response.blob();
    })
    .then((response) => {
      myImage.src = URL.createObjectURL(response);
    });    
});

fetch(myRequest)
  .then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${ response.status }`);
    }
    
    return response.blob();
  })
  .then((response) => {
    myImage.src = URL.createObjectURL(response);
  });