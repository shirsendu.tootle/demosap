$(document).on('click', '.btn', 'click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    jQuery.ajax({
        type: "GET",
        url: url,
        dataType: "html",
        success: function (response) {
            var newDoc = document.open("text/html", "replace");
            newDoc.write(response);
            newDoc.close();
            window.history.pushState({}, '', url);
        },
    });
});