$(document).on('click', '.btn', 'click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    jQuery.ajax({
        type: "DELETE",
        url: url,
        dataType: "json",
        success: function (response) {
            var error = response.error;
            var success = response.success;
            if (success === 0) {
                if (response.time) {
                    if (response.msg) {
                        errorMsg(
                            response.msg,
                            response.time
                        );
                    }
                    return false;
                }
                errorMsg(response.msg);
                return false;
            } else {
                if (response.time) {
                    if (response.msg) {
                        successMsg(
                            response.msg,
                            response.time
                        );
                    }
                } else {
                    if (response.msg) {
                        successMsg(response.msg);
                    }
                }

                if (response.hide) {
                    $(document).find(response.hide).hide();
                }

                if (response.load) {
                    $(document)
                        .find(response.load)
                        .load(" " + response.load + " > *");
                }

                if (response.show) {
                    $("html, body").animate(
                        { scrollTop: 0 },
                        "slow"
                    );
                    $(document).find(response.show).show();
                }

                if (response.refresh) {
                    if (response.refresh == 1) {
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    }
                }

                if (response.resetcaptcha) {
                    grecaptcha.reset();
                }

                setTimeout(() => {
                    if (response.show2) {
                        $(document)
                            .find(response.show2)
                            .show();
                    }
                }, 1000);

                $(":input", ".form")
                    .not(
                        ":button, :submit, :reset, :hidden"
                    )
                    .val("")
                    .prop("checked", false)
                    .prop("selected", false);
            }
            if (response.url) {
                setTimeout(function () {
                    window.location.href = response.url;
                }, 2000);
            }
        },
    });
});