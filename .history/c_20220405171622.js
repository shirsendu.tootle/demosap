$(document).on('click', '.btn', 'click', function(){});

fetch(myRequest)
  .then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${ response.status }`);
    }
    
    return response.blob();
  })
  .then((response) => {
    myImage.src = URL.createObjectURL(response);
  });